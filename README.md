# privacy-policy

My privacy policy, such as for my Android applications

Serge Masse
2022-11-24

For all my apps currently published, I do not sell any usage data, or any other kind of data. 

The data processed by my apps are private and owned by the user.

The apps contain no ads, no tracking by the apps.

Any apps distributed on the Google Play store, like mine are, do involve some tracking by Google, but there is no added tracking in my apps. The apps that are using Google Sign-In functions may also involve some tracking by Google and my apps do not have additional tracking in these cases either; the signed-in user ID, ususally a gmail address, may be included in autonomous emails send by the app, and this adds some security such as a deterrent to potential misuse of these app. For some apps, autonomous emails are sent via a gmail account that I manage and these emails are visible by me; for other apps that are sending autonomous emails, these may use a gmail account managed by a user organization and I may have no access. Manual emails sent by the user of my apps are not visible by me.
